# VirtualLab

VirtualBox small network management scripts

### Info

Jacek Łysiak jacek.lysiako.o@gmail.com

Bunch of scripts to manage small number of VirtualBox machines.  
Prepared during Distributed Systems course @ MIMUW 2017.  
Used for CloudAtlas distributed system developement (simplified clone of [Astrolabe](https://www.cs.cornell.edu/projects/quicksilver/public_pdfs/Astrolabe.pdf)).

### Software required:

- VirtualBox (scripts tested on VBox 5.0.40)
- sshpass

### Linux image requirements:

- Ubuntu/Debian distro with:
  - sshd
    - edit /etc/ssh/sshd\_confg and allow root login: `PermitRootLogin yes`
  - screen

##### Temporary note:
###### 29.12.2017

I can share my .ova image of Debian which I actually use.  
Some tools for automation of creating such image should be available in future.

### Functionalities

Detailed usage information is provided when script is fired without params (or in case of bad way of usage :)

- `vlab-setup` - import virtual machine `*.ova` image, setup account and passwords, setup internal network
- `vlab-start` - start selected machines in virtual lab
- `vlab-stop` - stop selected machines in virtual lab
- `vlab-clean` - delete all machines in virtual lab from VirtualBox registry
- `vlab-do` - execute bash commmands as regular user
- `vlab-do-as-root` - ... same but as root
- `vlab-upload` - upload given files into given destination directory onto given machines


